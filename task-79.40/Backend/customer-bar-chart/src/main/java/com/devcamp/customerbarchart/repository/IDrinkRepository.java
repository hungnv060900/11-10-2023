package com.devcamp.customerbarchart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerbarchart.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}

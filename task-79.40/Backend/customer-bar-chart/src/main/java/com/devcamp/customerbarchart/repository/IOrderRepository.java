package com.devcamp.customerbarchart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.customerbarchart.model.COrder;

@Repository
public interface IOrderRepository extends JpaRepository<COrder, Long> {
	COrder findByOrderCode(String orderId);
}
